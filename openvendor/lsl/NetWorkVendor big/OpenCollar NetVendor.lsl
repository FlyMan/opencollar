﻿// OpenCollar NetVendor - 0.924
string groupid = "45d71cc1-17fc-8ee4-8799-7164ee264811";
integer frontface = 0;
integer topleft = 0;//lowest numbered item (by texture num) currently being displayed
key BLANK = "9b17c673-e805-ed71-4e98-7bdbcb841140";
integer pagesize = 8;
integer g_iNum_Textures;
key rcpt;
key g_kVendor_Httpid;
key g_kTexture_Httpid;
string objname;

integer g_iUpdateIntervall = 86400; // intervall the server contacvht the db for new textures, for testing we use 3600 secind (1 hour)
integer g_iRecheckIntervall = 60; // intervall to wait if texture update is in progress
integer g_iResetOnErrorIntervall = 7200; // intervall for autoreset on error

integer g_iRecheckOnErrorIntervall = 300;
integer g_iLastRecheckOnError = 0;

list g_lTextureServerList;

string g_sTextureVersion="0";
//URL of the database app the vendor uses, in note is set it wil use  http://vendors.mycollar.org
string g_sBaseURL  = "http://vendors.mycollar.org";
string g_sTextureURL;

// Will the vendor post his location to the OpenCollar website, so it is accessible easy for other visitors, set to 0 to prevent that
integer g_iPublicListed=1;
string g_sVendorSLURL = "";
string g_sVendorParcelRating = "";
string g_sVendorParcelName = "";
string g_sVendorWebData = "";
key g_kSimDataRequest;

// Include or exclude mode, if vendor is in include mode, only items listed here wil be shown, in Exclude mode items lited her wil NOT be shown, all other will be
string g_sIncludeExcludeMode=""; //other option is exclude

//List of items to be included or exclude, the entry Item can be used multiple times.
list g_lItemList = [];

// Quiet mode doesnt use OwnerSay but for errors
integer g_iQuiet = 0;

// Globals Notecard reader
integer g_iNotecardIndex;
integer g_iNoteCardLine;
key g_kCurrentDataRequest;
string g_sSettingsNotecard = "!Settings!";


Talk(string sText)
{
    if (g_iQuiet ==0) llOwnerSay(sText);
}

string PrimName(integer link)
{
    return llList2String(llGetObjectDetails(llGetLinkKey(link), [OBJECT_NAME]), 0);
}

ShowTextures(integer start)
{
    //loop through all prims, displaying textures
    integer n;
    integer stop = llGetNumberOfPrims() + 1;
    for (n = 0; n < stop; n++)
    {
        string name = PrimName(n);
        if ((integer)name || name == "0")
        {
            if ( (start + (integer)name) >= llGetListLength(g_lTextureServerList) / 2 )
            {//no texture for this prim, set to blank
                llSetLinkPrimitiveParamsFast(n,  [PRIM_TEXTURE, frontface, BLANK, <1,1,0>, <0,0,0>, -PI_BY_TWO]);
            }
            else
            {
                key tex = (key)llList2String(g_lTextureServerList, (start + (integer)name)*2 + 1);
                llSetLinkPrimitiveParamsFast(n,  [PRIM_TEXTURE, frontface, tex, <1,1,0>, <0,0,0>, -PI_BY_TWO]);
            }
        }
    }
}
Debug(string sStr)
{
    llOwnerSay(sStr);
}

GenerateList()
{
    g_lTextureServerList = [];
    //    g_lTextureKeyList = [];
    integer i;
    for (i=0; i<llGetInventoryNumber(INVENTORY_TEXTURE); i++)
    {
        g_lTextureServerList += [llGetInventoryName(INVENTORY_TEXTURE, i)];
        g_lTextureServerList += [llGetInventoryKey(llGetInventoryName(INVENTORY_TEXTURE, i))];
    }
    g_iNum_Textures = llGetInventoryNumber(INVENTORY_TEXTURE);
}


// ----------------------------------------------------------------------
// --                           Notecard reader                        --
// ----------------------------------------------------------------------

// SampleSettingsReader.lsl
// Written by Konigmann Lippmann
// Thanks to Apotheus Silverman for his MultiItem Vendor...
// It seems like everything I write is based on it in one way or another.

// Here is a sample notecard
// ShowHoverText=True
// HoverTextValue=It has worked!
// Color=<1.0,1.0,1.0>

integer StringLeftICompare( string sLeftMatch, string sLongString )
{
    integer iLength;

    iLength = llStringLength( sLeftMatch ) - 1;
    if( llToLower(llGetSubString( sLongString, 0, iLength ) ) == llToLower(sLeftMatch) )
        return( TRUE );
    return( FALSE );
}


string GetValue( string sString )
{
    integer iStart;
    string sValue = "";
    string sbValue = "";

    iStart = llSubStringIndex( sString, "=" ) + 1;

    if (iStart >= llStringLength(sString))
    {
        return "";
    }
    else if( iStart )
    {
        sValue = llStringTrim(llGetSubString( sString, iStart, -1 ), STRING_TRIM);

        if( sValue )
        {
            sbValue = llToLower( sValue );
            if( sbValue == "true" )
                sValue = "1";
            if( sbValue == "false" )
                sValue = "0";
            return( sValue );
        }
    }
    return( NULL_KEY );
}

ShowSettings()
{
    llOwnerSay("Using the following settings:");
    llOwnerSay("Base URL: " + g_sBaseURL);
    llOwnerSay("Quiet Mode: " + (string)g_iQuiet);
    if (g_iPublicListed == 0)
    {
        llOwnerSay("Sending no SLULR to server, the location will NOT be published!");
    }
    else
    {
        llOwnerSay("Sending SLULR to server, this location will be published: '" + g_sVendorParcelName + "' " + g_sVendorSLURL + " (" +g_sVendorParcelRating+")");
    }

    if ((g_sIncludeExcludeMode=="include") || (g_sIncludeExcludeMode=="exclude"))
    {
        llOwnerSay("The vendor will " + g_sIncludeExcludeMode + " the following textures: " + llList2CSV(g_lItemList) +".");
    }
    else
    {
        llOwnerSay("The vendor will use all textures found on the DB.");
    }
}

GenerateParcelInfo()
{
    vector vPos = llGetPos() - (<4,0,0> * llGetRot());
    g_sVendorParcelName = llList2String(llGetParcelDetails(vPos, [PARCEL_DETAILS_NAME]), 0);
    g_sVendorSLURL = "http://slurl.com/secondlife/"+llEscapeURL(llGetRegionName())+"/"+(string)llRound(vPos.x)+"/"+(string)llRound(vPos.y)+"/"+(string)llRound(vPos.z)   ; //STRAY%20NATION/13/89/0
}

default
{
    state_entry()
    {
        llOwnerSay("\nWelcome to the OpenCollar Network Vendor.\n\nPlease place the vendor where you want it, set it to OpenCollar group and click it to connect to the database. The vendor will publish its SLURL to the database than and get the latest texture pack. \n\nIf you move it later to a new place, please reset it (via the SL Tools menu) so the SLURL on our database servers wil be updated. \n\nIf you don't want the SLURL of the vendor to be published, please edit the '!Settings!' notecard.");
        llSetText("Place the vendor and click it to start the setup ...",<1,1,0>,1);
        
    }
    
    touch_start(integer number)
    {
        if (llDetectedKey(0)==llGetOwner())
        {
            llOwnerSay("Starting the vendor ...");
            state startup;
        }
    }


    
    on_rez(integer param)
    {
        llResetScript();
    }
}

state startup
{
    on_rez(integer param)
    {
        llResetScript();
    }

    state_entry()
    {
        llOwnerSay("The vendor is inializing, please wait ...");
        llSetText("Initializing ...",<1,1,0>,1);
        g_lItemList = [];
        ShowTextures(3000);//blank all panes until we know we're set to the right group
        if (llList2String(llGetObjectDetails(llGetKey(), [OBJECT_GROUP]), 0) != groupid)
        {
            llOwnerSay("Sorry, but group-only vendors must be set to the OpenCollar group.");
            llSetText("Wrong group set, vendor stopped! Owner can click it to reset after the group has been changed.",<1,0,0>,1);
        }
        else
        {
            GenerateParcelInfo();
            g_kSimDataRequest = llRequestSimulatorData(llGetRegionName(), DATA_SIM_RATING);
        }
    }

    touch_start(integer number)
    {
        if (llDetectedKey(0)==llGetOwner())
        {
            llOwnerSay("Reseting the vendor ...");
            llResetScript();
        }
    }

    dataserver( key kQuery, string sData)
    {
        if (g_kSimDataRequest == kQuery)
        {
            g_sVendorParcelRating = sData;
            state ReadConfig;
        }
    }
}

state ReadConfig
{
    on_rez( integer param )
    {
        llResetScript();
    }

    state_entry()
    {
        Talk("The vendor is reading the config, please wait ...");
        llSetText("Reading the config, please wait ...",<1,1,0>,1);
        if( llGetInventoryType(g_sSettingsNotecard) == INVENTORY_NOTECARD )
        {
            //sSettingsNotecard = llGetInventoryName( INVENTORY_NOTECARD, iNotecardIndex );
            g_iNoteCardLine = 0;
            g_kCurrentDataRequest = llGetNotecardLine( g_sSettingsNotecard, g_iNoteCardLine );
            g_iNotecardIndex++;
        }
        else
        {
            llOwnerSay( "Settings notecard '" + g_sSettingsNotecard + "' not found, using default values." );
            state init;

        }
    }

    dataserver( key kQuery, string sData)
    {
        g_kCurrentDataRequest = "";
        if( sData != EOF )
        {
            sData = llStringTrim(sData, STRING_TRIM);
            llSetText("Reading the config, please wait ... line " + (string)g_iNoteCardLine ,<1,1,0>,1);

            if ((llGetSubString(sData,0,0) != "#") && (llGetSubString(sData,0,1) != "//"))
            {
                if( StringLeftICompare( "BaseURL=", sData ) )
                    g_sBaseURL = (string)GetValue( sData );

                else if( StringLeftICompare( "IncludeExcludeMode=", sData ) )
                {
                    g_sIncludeExcludeMode = llToLower((string)GetValue( sData ));
                    if (g_sIncludeExcludeMode == "off") g_sIncludeExcludeMode = "";
                }

                else if( StringLeftICompare( "PublicListed=", sData ) )
                {
                    g_iPublicListed = (integer)GetValue( sData );

                }
                else if( StringLeftICompare( "Quiet=", sData ) )
                    g_iQuiet = (integer)GetValue( sData );

                else if( StringLeftICompare( "Item=", sData ) )
                    g_lItemList += (string)GetValue( sData );

            }

            g_kCurrentDataRequest = llGetNotecardLine( g_sSettingsNotecard, ++g_iNoteCardLine );
        }
        else
        {
            g_iNotecardIndex++;
            if( g_iNotecardIndex < llGetInventoryNumber( INVENTORY_NOTECARD ) )
            {
                g_sSettingsNotecard = llGetInventoryName( INVENTORY_NOTECARD, g_iNotecardIndex );

                g_iNoteCardLine = 0;
                llGetNotecardLine( g_sSettingsNotecard, g_iNoteCardLine );
            }
            else
            {
                Talk("Note card read, now initializing ...");
                state init;
            }
        }
    }


    changed(integer change)
        // reset the script on any inventory change
    {

        if (change & CHANGED_INVENTORY)
        {
            Talk("Inventoy has changed, resetting the scripts ...");
            llResetScript();
        }
    }
}

// ----------------------------------------------------------------------
// --                       END Notecard reader END                    --
// ----------------------------------------------------------------------




state init
{

    state_entry()
    {
        if (g_iQuiet == 0)
            ShowSettings();
        Talk("Contacting DB for texure pack update ...");
        llSetText("Contacting DB for texure pack update ...",<1,1,0>,1);
        g_sTextureURL = g_sBaseURL + "/textureserver/";
        if (g_iPublicListed != 0)
        {
            g_sVendorWebData = g_sVendorSLURL + "\n" + g_sVendorParcelName + "\n" + g_sVendorParcelRating;
        }
        else
        {
            g_sVendorWebData = "";
        }
        g_kTexture_Httpid = llHTTPRequest(g_sTextureURL + "getalltextures?last_version=" + g_sTextureVersion + "&Public=" + (string)g_iPublicListed, [HTTP_METHOD, "POST"], g_sVendorWebData);

    }

    on_rez(integer n)
    {
        llResetScript();
    }


    changed(integer c)
    {
        if (c & CHANGED_INVENTORY)
        {
            llResetScript();
        }
    }

    http_response(key id, integer status, list meta, string body)
    {
        if (id == g_kTexture_Httpid)
        {
            if (status == 200)
            {
                if (body == "CURRENT")
                {
                    Talk("Textures are current, switching back to vendor mode!");
                    state ready;
                }
                if (body == "Updating")
                {
                    Talk("The texture server is just receiving new textures, update will be restarted in 1 minute!");
                    llSetTimerEvent(g_iRecheckIntervall);                }

                else
                {
                    //llOwnerSay("Length:"+(string)llStringLength(body) +"\n"+ body);
                    list lLines= llParseString2List(body,["\n"], []);
                    string sGetNext = "";
                    if ( (llList2String(lLines, 0) == "version") || (llList2String(lLines, 0) == "continue") )
                    {
                        if (llList2String(lLines, 0) == "version")
                        {
                            Talk("Textures need update to Texture pack version " + llList2String(lLines, 1));

                            g_lTextureServerList = [];
                            //                            g_lTextureKeyList = [];
                        }
                        else
                        {
                            //llOwnerSay("Receiving next package from Texture pack version " + llList2String(lLines, 1));
                        }
                        integer i;
                        for (i=2; i<llGetListLength(lLines); i += 2)
                        {
                            if (llList2String(lLines, i) == "end")
                            {
                                g_sTextureVersion = llList2String(lLines, 1);
                            }
                            else if (llList2String(lLines, i) == "startwith")
                            {
                                sGetNext = llList2String(lLines, i + 1);
                            }
                            else
                                // we have a texture, so now check against the list
                            {
                                string sItemName = llList2String(lLines, i);
                                string sItemTexture = llList2String(lLines, i+1);
                                if (g_sIncludeExcludeMode =="include")
                                    // include mode, so only add know textures
                                {
                                    if (llListFindList(g_lItemList,[sItemName]) >= 0)
                                        g_lTextureServerList += [llList2String(lLines, i),llList2String(lLines, i+1)];
                                }
                                else if (g_sIncludeExcludeMode=="exclude")
                                    // exclude mode, dont show know textures
                                {
                                    if (llListFindList(g_lItemList,[sItemName]) < 0)
                                        g_lTextureServerList += [llList2String(lLines, i),llList2String(lLines, i+1)];
                                }
                                else
                                    // unkown or off mode, so just add
                                {
                                    //llOwnerSay("0");
                                    g_lTextureServerList += [llList2String(lLines, i),llList2String(lLines, i+1)];
                                }
                            }
                        }
                        if (sGetNext != "")
                        {
                            //llOwnerSay("Getting next part of the texture pack");
                            g_kTexture_Httpid = llHTTPRequest(g_sTextureURL + "getalltextures?last_version=" + g_sTextureVersion + "&start=" + sGetNext + "&Public=" + (string)g_iPublicListed, [HTTP_METHOD, "POST"], g_sVendorWebData);
                        }
                        else
                        {
                            g_lTextureServerList = llListSort(g_lTextureServerList, 2, TRUE);
                            g_iNum_Textures = llGetListLength(g_lTextureServerList) / 2;
                            Talk("Finished updating the texture pack, " + (string)g_iNum_Textures + " textures loaded. Switching back to vendor mode");
                            ShowTextures(topleft);

                            state ready;
                        }

                    }
                    else
                    {
                        llOwnerSay("Unknown answer, status " + (string)status);
                        llOwnerSay(body);
                        llSetText("Error occured, vendor offline! Owner can touch to reset it!",<1,0,0>,1);
                        state error_mode;
                    }
                }
            }
            else
            {
                llOwnerSay((string)status);
                llOwnerSay(body);
                llSetText("Error occured, vendor offline! Owner can touch to reset it!",<1,0,0>,1);
                state error_mode;
            }
        }
    }

    timer()
    {
        llSetTimerEvent(0);
        Talk("Re-contacting DB for texure pack update ...");
        llSetText("Re-contacting DB for texure pack update ...",<1,1,0>,1);

        g_kTexture_Httpid = llHTTPRequest(g_sTextureURL + "getalltextures?last_version=" + g_sTextureVersion, [HTTP_METHOD, "POST"], "");

    }
}

state ready
{
    state_entry()
    {
        llSetText("",<1,1,1>,1);

        Talk("System in vendor mode");
        //        GenerateList();
        //        g_iNum_Textures = llGetInventoryNumber(INVENTORY_TEXTURE);
        //        g_kTexture_Httpid = llHTTPRequest(g_sTextureURL + "versioncheck?tv=" + g_sTextureVersion, [HTTP_METHOD, "POST"], "");
        llSetTimerEvent(g_iUpdateIntervall);

    }

    touch_start(integer t)
    {
        if (llDetectedGroup(0))
        {
            integer link = llDetectedLinkNumber(0);
            if (link == 1)
            {//clicked root prim.  invite to group
                string url = "secondlife:///app/group/" + groupid + "/about";
                llInstantMessage(llDetectedKey(0), "To join the OpenCollar Group, please Click this link:\n" + url);
            }
            else
            {
                string name = PrimName(link);
                if (name == "Prev")
                {//show prev page
                    topleft = topleft - pagesize;
                    if (topleft < 0)
                    {

                        topleft = g_iNum_Textures - (g_iNum_Textures % pagesize);
                    }
                    ShowTextures(topleft);
                }
                else if (name == "Next")
                {//show next page
                    topleft = topleft + pagesize;
                    if (topleft > g_iNum_Textures - 1)
                    {
                        topleft = 0;
                    }
                    ShowTextures(topleft);
                }
                else if ((integer)name || name == "0")
                {//one of the numbered panes was clicked, deliver collar
                    integer itemnum = (integer)name + topleft;
                    if (itemnum < g_iNum_Textures)
                    {
                        rcpt = llDetectedKey(0);
                        objname = llList2String(g_lTextureServerList, ((integer)name + topleft) * 2);
                        g_kVendor_Httpid = llHTTPRequest("http://collardata.appspot.com/dist/deliver", [HTTP_METHOD, "POST"], "objname=" + objname + "\nrcpt=" + (string)rcpt);
                    }
                }
            }
        }
        else
        {
            llInstantMessage(llDetectedKey(0), "Sorry, but you must be an OpenCollar group member to use this vendor.  Admission is free.  You can find the group in search, or click here: secondlife:///app/group/" + (string)groupid + "/about");
        }
    }

    http_response(key id, integer status, list meta, string body)
    {
        if (id == g_kVendor_Httpid)
        {
            if (status == 200)
            {
                llInstantMessage(rcpt, objname + " should be delivered in the next 30 seconds.");
            }
            else
            {

                llSay(0, "There was a problem with your delivery. Please try again or contact the support from OpenCollar.");
                llOwnerSay((string)status);
                llOwnerSay(body);
            }
        }
    }

    on_rez(integer num)
    {
        llResetScript();
    }

    changed(integer c)
    {
        if (c & CHANGED_INVENTORY)
        {
            llResetScript();
        }
    }

    timer()
    {
        llOwnerSay("Checking for updated textures. (Current version "+ g_sTextureVersion +")");
        state init;
    }

}

state error_mode
{
    state_entry()
    {
        llSetTimerEvent(g_iResetOnErrorIntervall);
    }

    touch_start(integer number)
    {
        if (llDetectedKey(0)==llGetOwner())
        {
            Talk("Reseting the vendor ...");
            llResetScript();
        }
    }

    timer()
    {
        Talk("Automatic reset of the vendor ...");
        state init;
    }

}
