//OpenCollar - inplaceUpdater - 3.525
//Licensed under the GPLv2, with the additional requirement that these scripts remain "full perms" in Second Life.  See "OpenCollar License" for details.

//in place updater

string RELESENOTE_URL = "http://code.google.com/p/opencollar/wiki/ReleaseNotes"; // ad the version in form of "XYYY" at the end
string ISSUETRACKER_URL = "http://code.google.com/p/opencollar/issues/list?can=2&q=milestone=Release"; // add the version in from of x.y at the end

integer updatechannel = -7483214;
integer listenhandle;
float version;
string versionstring;

list offering;
integer updatePin;
key myKey;
string hoverText = "PLEASE WAIT\n";

integer line;
key dataid;
string noteCard = "OldItemsToDelete";
list oldItemsToDelete;

string instructions;

key particletexture = "41873cb5-cb92-abca-16c4-ac319c9e2067";

integer debughandle;
string debugMessage;

integer g_nDoubleCheckChannel=-0x10CC011A; // channel for finding multiple updaters

key g_keyWearer; // id of wearer

integer g_iRecentlyTouched = FALSE;
float g_fSecondTouchDelay = 10.0;

debug(string str)
{
    //llOwnerSay(llGetScriptName() + ": " + str);
}

integer isUpdateManagerScript(string name)
{
    name = llList2String(llParseString2List(name, [" - "], []), 1);
    if (name == "updateManager")
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

UpdateParticle(key target, vector color)
{
    integer effectFlags;
    effectFlags = effectFlags|PSYS_PART_INTERP_COLOR_MASK;
    effectFlags = effectFlags|PSYS_PART_INTERP_SCALE_MASK;
    effectFlags = effectFlags|PSYS_PART_FOLLOW_SRC_MASK;
    effectFlags = effectFlags|PSYS_PART_FOLLOW_VELOCITY_MASK;
    effectFlags = effectFlags|PSYS_PART_TARGET_POS_MASK;
    effectFlags = effectFlags|PSYS_PART_EMISSIVE_MASK;
    llParticleSystem([
        PSYS_PART_FLAGS,            effectFlags,
        PSYS_SRC_PATTERN,           PSYS_SRC_PATTERN_DROP,
        PSYS_PART_START_COLOR,      color,
        PSYS_PART_END_COLOR,        color,
        PSYS_PART_START_ALPHA,      0.75,
        PSYS_PART_END_ALPHA,        0.25,
        PSYS_PART_START_SCALE,      <0.25,0.25,0.0>,
        PSYS_PART_END_SCALE,        <0.04,0.04,0.0>,
        PSYS_PART_MAX_AGE,          2.0,
        PSYS_SRC_ACCEL,             <0.0,0.0,0.0>,
        PSYS_SRC_TEXTURE,           particletexture,
        PSYS_SRC_BURST_RATE,        0.1,
        PSYS_SRC_INNERANGLE,        0.0,
        PSYS_SRC_OUTERANGLE,        0.0,
        PSYS_SRC_BURST_PART_COUNT,  1,
        PSYS_SRC_BURST_RADIUS,      0.0,
        PSYS_SRC_BURST_SPEED_MIN,   1.0,
        PSYS_SRC_BURST_SPEED_MAX,   2.0,
        PSYS_SRC_MAX_AGE,           0.0,
        PSYS_SRC_TARGET_KEY,        target,
        PSYS_SRC_OMEGA,             <0.0, 0.0, 0.0>   ]);
}

initiate()
{   //create a list of inventory types the updater has to offer the collar, store the updater version and open the listener
    // llSleep(2.0);
    line = 0;
    oldItemsToDelete = [];
    dataid = llGetNotecardLine(noteCard, line);
    llParticleSystem([]);
    llSetLinkColor(3, <1,0,0>, ALL_SIDES);
    integer n;
    list types = [INVENTORY_SCRIPT, INVENTORY_OBJECT, INVENTORY_NOTECARD, INVENTORY_TEXTURE, INVENTORY_ANIMATION, INVENTORY_SOUND, INVENTORY_LANDMARK];
    for (n = 0; n < llGetListLength(types); n++)
    {
        integer type = llList2Integer(types, n);
        if (llGetInventoryNumber(type) > 0)
        {
            if (type == INVENTORY_SCRIPT || type == INVENTORY_NOTECARD)
            {
                if (llGetInventoryNumber(type) > 1)
                {
                    offering += [type];
                }
            }
            else
            {
                offering += [type];
            }
        }
    }

    UnRunScripts();
    versionstring = llList2String(llParseString2List(llGetObjectDesc(), ["~"], []), 1);
    version = (float)versionstring;
    instructions = "OpenCollar Update - " + llGetSubString((string)version, 0, 4) + "\nTo update your collar (version 3.020 or later):\n1 - Rez it next to me.\n2 - Touch the collar and select Help/Debug->Update";
    llSetText(instructions, <1,1,1>, 1);
    myKey = llGetKey();
    listenhandle = llListen(updatechannel, "", "", "");
    debughandle = llListen(DEBUG_CHANNEL, "", "", "");
    instructions = "\nTo update your collar (version 3.020 or later):\n1 - Rez it next to me.\n2 - Touch the collar and select Help/Debug->Update";
    llOwnerSay(instructions);
}

UnRunScripts()
{
    // set all scripts in me to NOT RUNNING
    integer n;
    for (n = 0; n < llGetInventoryNumber(INVENTORY_SCRIPT); n++)
    {
        string script = llGetInventoryName(INVENTORY_SCRIPT, n);
        if (script != llGetScriptName())
        {
            if (llGetInventoryType(script) == INVENTORY_SCRIPT)
            {
                if(llGetScriptState(script))
                {
                    llSetScriptState(script, FALSE);
                }
            }
            else
            {
                //somehow we got passed a script we can't find.  Wait a sec and try again
                if (llGetInventoryType(script) == INVENTORY_SCRIPT)
                {
                    llSetScriptState(script, FALSE);
                }
                else
                {
                    llWhisper(DEBUG_CHANNEL, "Could not set " + script + " to not running.");
                }
            }
        }
    }
}

OfferUpdate(key id)
{
    UpdateParticle(id, <1,0,0>);
    llSetLinkColor(3, <1,0,1>, ALL_SIDES);
    llSetText(hoverText + "Preparing Update", <1,1,0>, 1);
    llDialog(llGetOwner(),"Update started, please wait until it finished completely.\nThis will take 1 to 5 minutes.", ["Ok"],-47114711);
    string message = "toupdate," + llDumpList2String(offering, ",");
    llWhisper(updatechannel, message);
}

GiveItemList(integer type)
{
    integer i;
    list items;
    for( i = 0; i < llGetInventoryNumber(type); i++)
    {
        if (type == INVENTORY_SCRIPT)
        {
            if (llGetInventoryName(type, i) != llGetScriptName())
            {
                items += llGetInventoryName(type, i);
            }
        }
        else if (type == INVENTORY_NOTECARD)
        {
            if (llGetInventoryName(type, i) != noteCard)
            {
                items += llGetInventoryName(type, i);
            }
        }
        else
        {
            items += llGetInventoryName(type, i);
        }
    }
    string myItems = llDumpList2String(items, ",");
    llWhisper(updatechannel, "items," + (string)type + "," + myItems);
}

CopyUpdateManager(key id, integer updatePin_local)
{
    string name;
    integer n;
    debug("sending updateManager script");
    for (n = 0; n < llGetInventoryNumber(INVENTORY_SCRIPT); n++)
    {
        name = llGetInventoryName(INVENTORY_SCRIPT, n);
        if(isUpdateManagerScript(name))
        {
            hoverText += ".";
            llSetText(hoverText + "\n" + name, <1,0,0>, 1);
            llRemoteLoadScriptPin(id, name, updatePin_local, TRUE, 42);
        }
    }
}

StartUpdate(key id, integer update)
{
    UpdateParticle(id, <1,1,1>);
    integer i;
    hoverText += "Updating Collar Items";
    llSetLinkColor(3, <0,0,1>, ALL_SIDES);
    llSetText(hoverText, <1,0,0>, 1);
    string name;
    debug("sending non script items");
    for (i = 0; i < llGetListLength(offering); i++)
    {
        integer type = llList2Integer(offering, i);
        if (type != INVENTORY_SCRIPT)
        {
            integer n;
            for(n = 0; n < llGetInventoryNumber(type); n++)
            {
                name = llGetInventoryName(type, n);
                if(name != noteCard)
                {
                    hoverText += ".";
                    llSetText(hoverText + "\n" + name, <1,0,0>, 1);
                    llGiveInventory(id, name);
                }
            }
        }
    }
    debug("sending scripts");
    for (i = 0; i < llGetInventoryNumber(INVENTORY_SCRIPT); i++)
    {
        name = llGetInventoryName(INVENTORY_SCRIPT, i);
        if (name != llGetScriptName() && !isUpdateManagerScript(name))
        {
            hoverText += ".";
            llSetText(hoverText + "\n" + name, <1,0,0>, 1);
            llRemoteLoadScriptPin(id, name, update, FALSE, 42);
        }
    }

    debug("done sending scripts, sending version command");
    //added a lil pause to let the update script deal with LMs
    llSleep(2.0);
    llWhisper(updatechannel, "version," + (string)version);
    UpdateParticle(id, <1,0,0>);
    llSetText("PLEASE WAIT\nFinalizing Update", <1,0,0>, 1);

}

default
    //state to eleminate multiple updaters
{
    state_entry()
    {
        g_keyWearer=llGetOwner();
        // init the double updater search
        llSetText("Updater is initalizing. Please wait ...",<1,0,0>,1.0);
        // listen on a channel
        llListen(g_nDoubleCheckChannel,"",NULL_KEY,"");
        // and say on the smae channel we are here
        llSay(g_nDoubleCheckChannel,"UpdateCheck:"+(string)llGetKey());
        // doublecheck with a sensor in casse lags eats us, listener i to avoid to many object near
        llSensor("", NULL_KEY, SCRIPTED, 15, PI);
        llSetTimerEvent(2.0);
    }

    on_rez(integer start_param)
    {
        llResetScript();
    }
    changed(integer change)
    {
        if(change & CHANGED_INVENTORY)
        {
            llSleep(2.0);
            llResetScript();
        }
    }

    listen(integer channel, string name, key id, string message)
    {
        if ((channel==g_nDoubleCheckChannel)&&(llGetOwnerKey(id)==llGetOwner()))
        {
            // we received our owner message back
            if (message=="UpdateCheck:"+(string)llGetKey())
            {
                // so we delete ourself
                llOwnerSay("There is more than one OpenCollar Collar Updater rezzed from you. Please use only one updater.");
                //Nan: this llDie() is an incredible pain in the ass when I'm trying to compare the contents of two updaters.  A warning is enough.  llDie is overkill.
                //Cleo: It leads to probles if we dotn kil it, we might change the updater method when the new updater is in place, for now we kil the updater again, if it doesnt have "text" in the name
                if (llSubStringIndex(llToLower(llGetObjectName()),"test")==-1)
                {
                    llDie();
                }
            }
            else
            {
                llSay(g_nDoubleCheckChannel,message);
            }
        }
    }

    sensor(integer num_detected)
    {
        // our sensor check, we check all object that are scripted
        integer i;
        for (i = 0; i < num_detected; i++)
        {
            if ((llDetectedKey(i) != llGetKey())&&(llGetOwnerKey(llDetectedKey(i))==llGetOwner()))
                // do they belong to us?
            {
                if (llGetSubString(llDetectedName(i), 0, 16) == "OpenCollarUpdater")
                    // now starts it with a name like ours?
                {
                    llOwnerSay("There is more than one OpenCollar Collar Updater rezzed from you. Please use only one updater.");
                    //Nan: this llDie() is an incredible pain in the ass when I'm trying to compare the contents of two updaters.  A warning is enough.  llDie is overkill.
                    //Cleo: It leads to probles if we dotn kil it, we might change the updater method when the new updater is in place, for now we kil the updater again, if it doesnt have "text" in the name
                    if (llSubStringIndex(llToLower(llGetObjectName()),"test")==-1)
                    {
                        llDie();
                    }
                }
            }
        }
    }


    timer()
    {
        // now answer from another upder, so all is good, back to normal updating mode
        state updating;
    }

}



state updating
{
    state_entry()
    {
        initiate();
        // we need to listen to the double check channel as long as  we exists
        llListen(g_nDoubleCheckChannel,"",NULL_KEY,"");
    }

    on_rez(integer start_param)
    {
        llResetScript();
    }
    changed(integer change)
    {
        if(change & CHANGED_INVENTORY)
        {
            llSleep(2.0);
            llResetScript();
        }
    }
    dataserver(key id, string data)
    {
        if (id == dataid)
        {
            if(data != EOF)
            {
                oldItemsToDelete += [data];
                line++;
                dataid = llGetNotecardLine(noteCard, line);
            }
            else
            {
                line = 0;
            }
        }
    }
    touch_start(integer num_detected)
    {
        if(llDetectedKey(0) == llGetOwner())
        {
            if (!g_iRecentlyTouched)
            {
                llOwnerSay("Have me close to your OpenCollar version 3.020 or higher and select in the Collar's Help/Debug menu Update.\nIf your version is 3.525 or higher, touch this orb again before "+(string)((integer)g_fSecondTouchDelay)+" seconds to start updating.");
                g_iRecentlyTouched = TRUE;
                llSetTimerEvent(g_fSecondTouchDelay);
            }
            else
            {
                integer iChan = (integer)("0x"+llGetSubString((string)g_keyWearer,2,7)) + 1111;
                if (iChan>0) iChan=iChan*(-1);
                if (iChan > -10000) iChan -= 30000;
                llWhisper(iChan, "update");
            }
        }
    }
    listen(integer channel, string name, key id, string message)
    {
        if (llGetOwnerKey(id) == llGetOwner()) //collar has to have the same owner as the updater!
        {
            if (channel==g_nDoubleCheckChannel)
                // we still check the double updater channel
            {
                // abnd replay if we get a message here
                llSay(g_nDoubleCheckChannel,message);
            }
            else if(channel == DEBUG_CHANNEL)
            {
                debugMessage = message;
            }
            else
            {
                debug(message);
                list temp = llParseString2List(message, ["|"], []);
                string command0 = llList2String(temp,0);
                string command1 = llList2String(temp,1);
                if (command0 == "UPDATE")
                {// Collar responded with update
                    if ((float)command1 > 3.221 && (float)command1 < version)
                    {//Collar version is at least 3.000 and lower than to Update version
                        llWhisper(updatechannel, "get ready");
                    }
                    else if ((float)command1 > 3.019 && (float)command1 < version)
                    {//Collar version is at least 3.000 and lower than to Update version
                        llWhisper(updatechannel, "items,0,0");
                    }
                    else if ((float)command1 < 3.000 )
                    {
                        llWhisper(updatechannel, "nothing to update");
                        llOwnerSay("Your OpenCollar is previous version 3 and cannot be updated this way, please get an OpenCollar version 3 or higher first.");
                    }
                    else if ((float)command1 >= version)
                    {
                        llWhisper(updatechannel, "nothing to update");
                        llOwnerSay("Your OpenCollar is the same or newer version, nothing to update.");
                    }
                }
                else if (command0 == "ready")
                {// Collar responed everything is ready, douplicate items were deleted so start to send stuff over
                    updatePin = (integer)command1;
                    CopyUpdateManager(id, updatePin);
                    // StartUpdate(id, (integer)command1, TRUE);
                }
                else if (message == "Manager Ready")
                {
                    if (llGetListLength(oldItemsToDelete) > 0 && llToLower(llList2String(oldItemsToDelete, 0)) != "nothing")
                    {
                        llWhisper(updatechannel, "delete," + llDumpList2String(oldItemsToDelete, ","));
                    }
                    else
                    {
                        OfferUpdate(id);
                    }
                }
                else if (message == "deletedOld")
                {
                    OfferUpdate(id); // give a list of item types to update
                }
                else if (command0 == "giveList")
                {// Collar requested a list of items of one type, send a list of item that will be copied
                    GiveItemList((integer)command1);
                }
                else if (message == "ready to receive")
                {
                    StartUpdate(id, updatePin);
                }
                else if (message == "copying child scripts")
                {
                    llSetText("PLEASE WAIT\nFinalizing Update\nCopying Scripts to Childprims", <1,0,0>, 1);
                }
                else if (message == "restarting collar scripts")
                {
                    llSetText("PLEASE WAIT\nFinalizing Update\nRestarting OpenCollar scripts.", <1,0,0>, 1);
                }
                else if (command0 == "finished")
                {// lets be sure also the update script is resetted before showing this
                    llSleep(2.0);
                    llSetLinkColor(3, <0,1,0>, ALL_SIDES);
                    llSetText("Update Finished.\nYour collar has been updated to:\n" + llGetSubString((string)version, 0, 4), <0,1,0>, 1);
                    hoverText = "PLEASE WAIT\n";
                    llParticleSystem([]);
                    //get the info about the version and show the user a appropiate wiki page:
                    string sURL;
                    string sInfo;
                    integer iMainVersion = (integer)llGetSubString(versionstring,0,0);
                    integer iMinorVersion = (integer)llGetSubString(versionstring,2,2);
                    integer iSubVersion = (integer)llGetSubString(versionstring,3,4);
                    if (iSubVersion<20)
                    {
                        // normal version
                        if (iSubVersion == 0)
                        {
                            sURL = RELESENOTE_URL + llGetSubString(versionstring,0,0) + llGetSubString(versionstring,2,2);

                        }
                        else
                        {
                            sURL = RELESENOTE_URL + llGetSubString(versionstring,0,0) + llGetSubString(versionstring,2,2) + llGetSubString(versionstring,3,4);
                        }
                        sInfo = "\nYou find the release notes at our Wiki page.";
                    }
                    else
                    {
                        integer iMilestone = (integer)llGetSubString(versionstring,2,2);
                        sURL = ISSUETRACKER_URL + llGetSubString(versionstring,0,0) + "." + (string)(iMilestone + 1);
                        sInfo = "\nAll issue for the upcoming release can be found at our issue tracker.";
                    }
                    llLoadURL(llGetOwner(),"Update finished!\nYour collar has been updated to:\n" + versionstring + sInfo, sURL);

                    if(debugMessage != "")
                    {
                        llOwnerSay("You may have seen an error:\n \"" + debugMessage + "\".\nThis should be nothing to worry. You may point the designer of this collar to it though.");
                    }
                }
            }
        }
    }

    timer()
    {
        llSetTimerEvent(0);
        g_iRecentlyTouched = FALSE;
    }
}