# Cleo: I suggest to remove comment starting with Cleo


#!/usr/bin/python
#Licensed under the GPLv2 (not later versions)
#see LICENSE.txt for details

import cgi
import os
import re
import logging
from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.api import memcache
from google.appengine.api.labs import taskqueue
from google.appengine.api import urlfetch

class Av(db.Model):
    id = db.StringProperty()
    name = db.StringProperty()

class Relation(db.Model):
    subj_id = db.StringProperty()
    type = db.StringProperty()
    obj_id = db.StringProperty()

class AppSettings(db.Model):
  #token = db.StringProperty(multiline=False)
  value = db.StringProperty(multiline=False)
  
sharedpass = AppSettings.get_or_insert("sharedpass", value="sharedpassword").value
cmdurl = AppSettings.get_or_insert("cmdurl", value="http://yourcmdapp.appspot.com").value  


relationtokens = {"owner":"owns", "secowners":"secowns"}#watch for these being saved, and make relations for them

allowed_quota=2048; # maximum allowed amount of data per user, might need to be adopted for 3.4 if we offer storage of bigger values via separate script




class MainPage(webapp.RequestHandler):
    def get(self):
        q = Relation.all()

        # If the app stored a cursor during a previous request, use it.
        last_cursor = self.request.get('cursor')
        if last_cursor:
            q.with_cursor(last_cursor)
        
        # Perform the query to get 100 results.
        relationships = q.fetch(20)
        
        # Store the latest cursor for the next request.
        cursor = q.cursor()
        taskqueue.Task(name=str(relationships[0].key()), url='/', params={'cursor': cursor}).add(queue_name='relationshiptran')
        for r in relationships:
            rpc = urlfetch.create_rpc()
            urlfetch.make_fetch_call(rpc, cmdurl + '/relation/?%s/%s/%s' % (r.type, r.obj_id, r.subj_id), method="PUT", headers={'sharedpass': sharedpass})
 
                
    def post(self):
        q = Relation.all()

        # If the app stored a cursor during a previous request, use it.
        last_cursor = self.request.get('cursor')
        if last_cursor:
            q.with_cursor(last_cursor)
        
        # Perform the query to get 100 results.
        relationships = q.fetch(20)
        
        # Store the latest cursor for the next request.
        cursor = q.cursor()
        if self.request.get('X-AppEngine-TaskRetryCount') == 0:
            taskqueue.Task(name=str(relationships[0].key()), url='/', params={'cursor': cursor}).add(queue_name='relationshiptran')
        
        for r in relationships:
            rpc = urlfetch.create_rpc()
            urlfetch.make_fetch_call(rpc, cmdurl + '/relation/?%s/%s/%s' % (r.type, r.obj_id, r.subj_id), method="PUT", headers={'sharedpass': sharedpass})

application = webapp.WSGIApplication(
    [('/.*', MainPage)], 
    debug=True) 

def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()
